# Max Filter

## Vision

The objective of this project is to create a spam filter using neural networks

## Milestones

1. Read my email __[x]__
2. Parse every email __[x]__
3. Detect if the parsed email is spam or not __[x]__
4. Create report about the emails which are spam the ones which arent __[x]__
5. Send back the report to my email __[x]__
6. Refactor the code after learning python better __[ ]__
7. Make the documentation in English __[ ]__

## Run The Spam Filter

* Select the options you want, and then it will read your email and try to detect the ones that are spam!

```py
python Implementação/Projecto_IC_21220124.py
```

## Base Reference

I used as reference a pos-graduation [report](https://www.ppgee.ufmg.br/documentos/Defesas/650/Lelia-Dissertacao-2006.pdf) about spam filters with neural networks and realized comparisons to its percepton and MLP results.

## Spam Datasets 

The used spam dataset was the [SpamBase](https://archive.ics.uci.edu/ml/datasets/Spambase).

## Other Interesting Sources

* [Jim Ramices](http://docplayer.com.br/17206228-Resumo-1-introducao-jim-ramices-das-ufsc-br.html)
* [Rui Vaz](https://repositorium.sdum.uminho.pt/bitstream/1822/19599/1/Thesis_Rui Fernando Martins Vaz.pdf)

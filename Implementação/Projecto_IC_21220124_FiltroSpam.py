# -*- coding: utf-8 -*-
"""
Spyder Editor

This temporary script file is located here:
C:\Users\Rui Margarido\.spyder2\.temp.py
"""
from __future__ import print_function
import poplib
import numpy
import theano
from pylearn2.models import mlp
from pylearn2.termination_criteria import EpochCounter
from pylearn2.training_algorithms import sgd
from pylearn2.datasets import dense_design_matrix as ddm
import pickle
from sklearn.linear_model import Perceptron
from email import parser
from bs4 import BeautifulSoup
from nltk import word_tokenize
from random import shuffle
from sklearn.metrics import mean_squared_error
from math import sqrt
import smtplib
from email.mime.text import MIMEText
#import getpass

#from nltk import PorterStemmer
#from pprint import pprint


MINWORDSIZE = 0
MAXWORDSIZE = 20
NPalavrasToken = 1
CARACTERESESPECIAISREMOVER = ["=","''","``"] #"''","``"
CaracteresAcentuadosAntigos = ["=E1","=E7","=E9","=EA","=ED","=F5","=F3", "=C9", "=A9", "=E3","=E2"]
CaracteresAcentuadosNovos = ["a","c","e","e","i","o","o","E","©","a","a"]
ListaPalavras = ["make","address","all","3d","our","over","remove","internet","order","mail","receive","will","people","report","addresses","free","business","email","you","credit","your","font","000","money","hp","hpl","george","650","lab","labs","telnet","857","data","415","85","technology","1999","parts","pm","direct","cs","meeting","original","project","re","edu","table","conference",";","(","[","!","$","#"]

def leCaixaEmail():    
    data = []    
    assuntos = []
    print("Nota: precisa ativar o pop3 e o smtp nas configuracoes do seu gmail")
    username = raw_input("Introduza o seu gmail   : ")
    username = username.strip()
    password = raw_input("Introduza a sua password   : ")
    password = password.strip()      
    pop_conn = poplib.POP3_SSL('pop.gmail.com')    
    pop_conn.user(username)
    pop_conn.pass_(password)
    #Get messages from server:
    messages = [pop_conn.retr(i) for i in range(1, len(pop_conn.list()[1]) + 1)]
    # Concat message pieces:
    messages = ["\n".join(mssg[1]) for mssg in messages]
    #Parse message intom an email object:
    messages = [parser.Parser().parsestr(mssg) for mssg in messages]
    i = 1
    for message in messages:
    
      #nome = "emails_ic\(%d)_body.txt" % i
      #nome2 = "emails_ic\(%d)_header.txt" % i
      f1 = open("temp.txt", 'w')  
      #f2 = open(nome2, 'w')
      
      #lines_of_text = [message['Subject'], "\n" ,message['Date'] , "\n" , message['From'] , "\n"] 
      #f2.writelines(lines_of_text)
      assuntos.append(message['Subject'])
      #print message['Date']
      #print message['From']  
      if message.is_multipart():
          for payload in message.get_payload():

              print(payload.get_payload(), file = f1)
                    
      else:
          print(message.get_payload(),file = f1)
      
      f1.close()
      data.append(preProcessamento())
    pop_conn.quit()
    print("\nEmails guardados em memoria e ja transformados em data")
    print("Escolha a rede com que pretende filtrar os seus emails")
    print("1 - Perceptron")
    print("2 - MLP")
    opcao = raw_input("Escolha   -> ")    
    if opcao == "1":
        result = predicaoPerceptron(data)
    if opcao == "2":
        result = predicaoMLP(data)
    f2 = open("Email.txt","w")
    i=0
    for subject in assuntos:
        f2.write("Assunto : " + subject +" : Resultado -> " + str(result[i]) + "\n")
        i+=1
    enviaEmailReport(username,password)
    return "unknown"
    
def remove_values_from_list(list, val):
    return [value for value in list if value != val]


def concatena_lista_strings(list):
    str = ""
    espaco = " "    
    return [str + espaco + i for i in list]


def removeLixo(list):    
    for lixo in CARACTERESESPECIAISREMOVER:
        i = 0        
        for str in list:
            list[i] = str.replace(lixo, "")
            i=i+1
    return list    


def trocaCaracteresEspeciais(list):
    j = 0    
    for caracter in CaracteresAcentuadosAntigos:
        i = 0
        for str in list:
            list[i] = str.replace(caracter, CaracteresAcentuadosNovos[j])
            i=i+1
        j = j+1
    return list
    
    
def acertaTokens(list):
    novaLista = []    
    j = 0
    f = 0    
    for str in list:
        if f == 1:
            f = 0
        else:
            i = str.find("=")
            
            if i != -1:
                if j != len(list):                                
                    novaLista.append(list[j] + list[j+1])
                j=j+1
                f=1
            else:
                novaLista.append(list[j])
            j = j+1
    return novaLista

    
def cortaListaMinMax(list, min, max):
    novaLista = []
    for str in list:
        if len(str) > min:
            if len(str) < max:
                novaLista.append(str)
    return novaLista

def criaExpressoesTokens(list, num):
    novaLista = []
    initMin = 0
    initMax = 0 + num
    while initMin < len(list):
        novaLista.append(" ".join(list[initMin:initMax]))
        initMin += num
        initMax += num
        if(initMax > len(list)):
            initMax = len(list)
    return novaLista
    
def preProcessamento():
    lista_filtrada = []    
    f1 = open("temp.txt","r")    
    body = f1.read()
    raw = BeautifulSoup(body).get_text()
    tokens = word_tokenize(raw)
    #remove_values_from_list(tokens, "u")
            
    for token in tokens:
        lista_filtrada.append(token.encode("utf-8"))        
        
            
    lista_filtrada = trocaCaracteresEspeciais(lista_filtrada)    
    #lista_filtrada = acertaTokens(lista_filtrada)    
    lista_filtrada = removeLixo(lista_filtrada)    
    lista_filtrada = cortaListaMinMax(lista_filtrada, MINWORDSIZE, MAXWORDSIZE)     
    mail = " ".join(lista_filtrada)    
      
    vector = fromMailToData(mail)    
      
    #lista_filtrada = criaExpressoesTokens(lista_filtrada, NPalavrasToken)
    
    #for str in lista_filtrada:
        #print(PorterStemmer().stem_word(str))    
    
    #print(' '.join(lista_filtrada))
    #print(lista_filtrada)    
        
    """for line in open("(1)_body.txt", 'r'):
        line = line.strip()
        line.split(" ")
        print(line)"""
        
    return vector
        

def loadDataset():   
    linha = 0
    dataset = []
    target = []
    dataset_teste = []
    target_teste = []
    dados = []
    for l in open("spambase.data" , "r"):
        dados.append(l)
    shuffle(dados)
    for line in dados:       
        line = line.strip()
        line = line.split(",")        
        data = []        
        pos = 0        
        for num in line:
            if pos < 57:            
                data.append(float(num))
            else:
                target.append(float(num))
            pos+=1
        dataset.append(data)
        linha+=1
    dataset = numpy.asarray(dataset)
    linha=0
    for line in open("spamteste.data" , "r"):       
        line = line.strip()
        line = line.split(",")        
        data = []        
        pos = 0        
        for num in line:
            if pos < 57:            
                data.append(float(num))
            else:
                target_teste.append(float(num))
            pos+=1
        dataset_teste.append(data)
        linha+=1
    dataset = numpy.asarray(dataset)
    #target = numpy.asarray(target)
    #target = target.reshape(target.shape[0],1)
    return dataset,target,dataset_teste,target_teste
        

#rotated90 = zip(*dataset[::-1])
#result = p.fit(rotated90,target)
#print(result)
def processaDatasetPerceptron(dataset, target,dataset_teste,target_teste):
    p = Perceptron(n_iter=1000, verbose=0,shuffle=True , random_state=None, fit_intercept=True, eta0=0.002)    
    p.fit(dataset,target)
    #result.append(p.score(dataset,target))
    result = p.predict(dataset_teste)
    result_imprimivel = []
    f1 = open("predicao.txt" , "w")
    f2 = open("target.txt", "w")   
    i = 0
    count = 0
    FP = 0
    FN = 0     
    for num in result:
        result_imprimivel.append(num)
    for num in result_imprimivel:
        if num == target_teste[i]:
            count+=1
        else:
            if target_teste[i] == 0:
                FP +=1
            if target_teste[i] == 1:
                FN +=1
        i+=1
    #print(result_imprimivel, file = f1)
    #print(target_teste, file = f2)
    print("Accuracy   " + str(p.score(dataset_teste, target_teste)*100) + "%")
    rms = sqrt(mean_squared_error(target_teste,result_imprimivel))
    print("Erro Quadratico medio   : " + str(rms))
    print("Falsos Positivos   : " + str(float(FP)/float(i)*100))
    print("Falsos Negativos   : " + str(float(FN)/float(i)*100))
    resposta = raw_input("Pretende guardar esta rede?\nIntroduza 1 se sim e outro valor qualquer se nao : ")
    if resposta == "1":    
        nome_fich = raw_input("Introduza o nome do ficheiro que sera criado(sem extensao)\n Nota: se o nome do ficheiro ja existir o ficheiro sera sobreescrito: ")        
        with open(nome_fich+".pkl", 'wb') as output:
           pickle.dump(p, output, pickle.HIGHEST_PROTOCOL)
    f1.close()
    f2.close()
    

def predicaoPerceptron(dataset):
    lista = []    
    for line in open("RedesAtivas.txt","r"):
        line = line.strip()
        lista.append(line)
    with open(lista[0], 'rb') as input:
        p = pickle.load(input)
        result = p.predict(dataset)    
    return result
    
def predicaoMLP(dataset):
    lista = []  
    result = []
    for line in open("RedesAtivas.txt","r"):
        line = line.strip()
        lista.append(line)    
    with open(lista[1], 'rb') as input:
            ann = pickle.load(input)
    inputs = numpy.array(dataset)
    result_mlp = ann.fprop(theano.shared(inputs, name='inputs')).eval()  
    for num1,num2 in result_mlp:
        if float(round(num2)) == 0.:
            result.append(0)
        else:
            result.append(1)
    return result
    
def carregaRedePerceptronGuardada(dataset,target):
    p = None
    nome_fich = raw_input("\nIntroduza o nome da rede que pretende utilizar(sem formato): ")    
    try:    
        with open(nome_fich+'.pkl', 'rb') as input:
            p = pickle.load(input)
        result = p.predict(dataset)
        result_imprimivel = []
        i = 0
        count = 0
        FP = 0
        FN = 0        
        #f1 = open("predicao.txt" , "w")
        #f2 = open("target.txt", "w")
        for num in result:
            result_imprimivel.append(num)
        for num in result_imprimivel:
            if num == target[i]:
                count+=1
            else:
                if target[i] == 0:
                    FP +=1
                if target[i] == 1:
                    FN +=1
            i+=1
        #print(result_imprimivel, file = f1)
        #print(target, file = f2)
        print("Accuracy   " + str(p.score(dataset, target)*100) + "%")
        rms = sqrt(mean_squared_error(target,result_imprimivel))
        print("Erro Quadratico medio   : " + str(rms))
        print("Falsos Positivos   : " + str(float(FP)/float(i)*100))
        print("Falsos Negativos   : " + str(float(FN)/float(i)*100))
    except:
        print("Erro na abertura de ficheiro")
    
    
def processaDatasetMLP(data, target,dataset_teste,target_teste):   
    targ = []
    targ_teste = []    
    for num in target:
        if num == 1. :
            targ.append([0,1])
        else:
            targ.append([1,0])
    for num in target_teste:
        if num == 1. :
            targ_teste.append([0,1])
        else:
            targ_teste.append([1,0])
            
    #start = 1
    #saturate = 20
    #decay_factor = .001
    #learning_rate_adjustor = sgd.LinearDecayOverEpoch(start, saturate, decay_factor)
    
    data = ddm.DenseDesignMatrix(X=numpy.array(data), y=numpy.array(targ))
    #data_test = ddm.DenseDesignMatrix(X=numpy.array(dataset_teste), y=numpy.array(targ_test))
    hidden_layer = mlp.Sigmoid(layer_name='hidden', dim=57 , irange=.1, init_bias=0.5)
    output_layer = mlp.Softmax(2, 'output', irange=.1)        
    trainer = sgd.SGD(learning_rate=.001, batch_size=57, termination_criterion=EpochCounter(100))    
    layers = [hidden_layer, output_layer]
    ann = mlp.MLP(layers, nvis=57)        
    trainer.setup(ann, data)
        
    rms = 1.0 
    while True:
        trainer.train(dataset=data)
        ann.monitor.report_epoch()
        ann.monitor()
        print("Erro    : " + str(rms))
        if not trainer.continue_learning(ann):
            break;
        #learning_rate_adjustor.on_monitor(ann, data, trainer)
        inputs = numpy.array(dataset_teste)
        result = ann.fprop(theano.shared(inputs, name='inputs')).eval()    
        rms = sqrt(mean_squared_error(targ_teste,result))
    noSpam = 0
    Spam = 0
    FP = 0
    FN = 0
    i = 0
    for num,num2 in result:
        if float(round(num)) == targ_teste[i][0]: 
                if float(round(num2)) == targ_teste[i][1]:
                    if targ_teste[i][1] == 0:
                        noSpam += 1
                    if targ_teste[i][1] == 1:
                        Spam += 1
        else:
            if targ_teste[i][1] == 0:
                FP += 1
            if targ_teste[i][1] == 1:
                FN += 1
        i+=1
    accuracy = (float(Spam + noSpam)/float(i)*100)
    print("Accuracy    : " + str(accuracy) + "%")
    rms = sqrt(mean_squared_error(targ_teste,result))
    print("Erro Quadratico medio   : " + str(rms))
    print("Falsos Positivos   : " + str(float(FP)/float(i)*100))
    print("Falsos Negativos   : " + str(float(FN)/float(i)*100))
    resposta = raw_input("Pretende guardar esta rede?\nIntroduza 1 se sim e outro valor qualquer se nao : ")
    if resposta == "1":    
        nome_fich = raw_input("Introduza o nome do ficheiro que sera criado(sem extensao)\n Nota: se o nome do ficheiro ja existir o ficheiro sera sobreescrito: ")        
        with open(nome_fich+".pkl", 'wb') as output:
           pickle.dump(ann, output, pickle.HIGHEST_PROTOCOL)
    #print(str(ann.cost(targ_test,outputs)))
    
    
def carregaRedeMLPGuardada(dataset,target):
    nome_fich = raw_input("\nIntroduza o nome da rede que pretende utilizar(sem formato): ")    
    try:    
        with open(nome_fich+'.pkl', 'rb') as input:
            ann = pickle.load(input)    
        target_teste = []    
        noSpam = 0
        Spam = 0
        FP = 0
        FN = 0        
        for num in target:
            if num == 1. :
                target_teste.append([0,1])
            else:
                target_teste.append([1,0])    
        inputs = numpy.array(dataset)
        result = ann.fprop(theano.shared(inputs, name='inputs')).eval()    
        i = 0  
        for num,num2 in result:
            if float(round(num)) == target_teste[i][0]: 
                if float(round(num2)) == target_teste[i][1]:
                    if target_teste[i][1] == 0:
                        noSpam += 1
                    if target_teste[i][1] == 1:
                        Spam += 1
            else:
                if target_teste[i][1] == 0:
                    FP += 1
                if target_teste[i][1] == 1:
                    FN += 1
            i+=1
        accuracy = (float(noSpam + Spam)/float(i)*100)
        print("Accuracy    : " + str(accuracy) + "%")
        rms = sqrt(mean_squared_error(target_teste,result))
        print("Erro Quadratico medio   : " + str(rms))
        print("Falsos Positivos   : " + str(float(FP)/float(i)*100))
        print("Falsos Negativos   : " + str(float(FN)/float(i)*100))
    except:
        print("Erro na abertura do ficheiro")
    

def processaDatasetFuzzy(dataset, target):
    return "unknown"
    
def menu_inicial():
    print("\n\t\t NO SPAM DAY! \n")
    print("1 - Filtrar endereco de email(Atualmente apenas funcional para gmail)")
    print("2 - Gestao interna aplicacao")
    print("3 - Sair")
    id = raw_input("Escolha o numero de uma das opcoes   : ")    
    return id
   
def menu():
    print("\nPara Todas as operacoes seguintes e necessario ter um dataset carregado")
    print("1 - Carregar dataset predefinido (spambase)\n")
    print("2 - Processa dados criando uma nova rede Perceptron")
    print("3 - Utiliza uma rede Perceptron guardada para prever os resultados")
    print("4 - Processa dados criando uma nova rede MLP")
    print("5 - Utiliza uma rede MLP guardada para prever os resultados\n")
    print("6 - Alterar as redes selecionadas para filtro de email")
    print("7 - Verificar redes atualmente selecionadas para filtro de email")
    print("8 - Volta ao menu principal")
    id = raw_input("Escolha o numero de uma das opcoes  : ")
    return id 
    
def verificaRedesAtivas():
    i = 0    
    for line in open("RedesAtivas.txt","r"):
        line = line.strip()
        if i == 0:
            print("Perceptron :  Nome Ficheiro -> " + line)
        if i == 1:
            print("MLP :  Nome Ficheiro -> " + line)
        i+=1

def alteraRedesAtivas():
    verificaRedesAtivas()
    lista = []    
    for line in open("RedesAtivas.txt","r"):
        line = line.strip()        
        lista.append(line)
    print("1 - Perceptron")
    print("2 - MLP")
    print("Qualquer outro valor para voltar ao menu anterior")
    opcao = raw_input("Qual das redes pretende alterar?   : ")
    if opcao == "1":
        nome = raw_input("Introduza o nome completo do ficheiro que possui os dados da rede Perceptron   : ")
        lista[0] = nome
        f1 = open("RedesAtivas.txt","w")
        f1.write(lista[0] + "\n")
        f1.write(lista[1] + "\n")
    if opcao == "2":
        nome = raw_input("Introduza o nome completo do ficheiro que possui os dados da rede MLP   : ")
        lista[1] = nome
        f1 = open("RedesAtivas.txt","w")
        f1.write(lista[0] + "\n")
        f1.write(lista[1] + "\n")
def devolveNumMedioMaiusculasSeguidas(email):
    conta = 0
    n_sequencias = 0    
    acumulador = 0

    for palavra in email:
        for letra in palavra:
            if letra.isupper():
                if conta > 1:
                    acumulador+=1
                if conta == 1:
                    n_sequencias+=1
                    acumulador+=2
                    conta +=1
                if conta == 0:
                    conta+=1
            else:
                conta=0
    if n_sequencias == 0:
        return 0
    else:
        return float(acumulador)/float(n_sequencias)    
    
def devolveMaiorSequenciaMaiusculas(email):
    conta = 0
    maior = 0
    for palavra in email:
        for letra in palavra:
            if letra.isupper():
                conta +=1
                if conta > maior:
                    maior = conta
            else:
                conta=0
    return float(maior) 

def contaLetrasMaiusculas(email):
    conta = 0
    for palavra in email:
        for letra in palavra:
            if letra.isupper():
                conta+=1
    return float(conta)

def fromMailToData(email):
    lista = []
    for palavra in ListaPalavras:    
        lista.append(float(email.count(palavra))/float(len(email))*100)
    lista.append(devolveNumMedioMaiusculasSeguidas(email))
    lista.append(devolveMaiorSequenciaMaiusculas(email))
    lista.append(contaLetrasMaiusculas(email))    

    return lista

def enviaEmailReport(username,password):
    fp = open("Email.txt", 'rb')
    msg = MIMEText(fp.read())
    fp.close()
    
    msg['Subject'] = "Relatorio de SPAM"
    msg['From'] = username
    msg['To'] = username
    
    s = smtplib.SMTP('smtp.gmail.com', 25)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(username,password)
    s.sendmail(msg['From'], msg['To'], msg.as_string())
    s.quit()    
    
while True:
    opcao_inicial=0    
    opcao=0
    opcao_inicial = menu_inicial()
    if opcao_inicial == "1":
        leCaixaEmail()
    if opcao_inicial == "2":
        opcao = menu()
        if opcao == "1":
            dataset,target,dataset_teste,target_teste = loadDataset()
        if opcao == "2": 
            processaDatasetPerceptron(dataset,target,dataset_teste,target_teste)
        if opcao == "3":
            carregaRedePerceptronGuardada(dataset_teste,target_teste)
        if opcao == "4":    
            processaDatasetMLP(dataset,target,dataset_teste,target_teste)
        if opcao == "5":
            carregaRedeMLPGuardada(dataset_teste,target_teste) 
        if opcao == "6":
            alteraRedesAtivas()
        if opcao == "7":
            verificaRedesAtivas()
    if opcao_inicial == "3":
        break;
